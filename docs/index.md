<!--
SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
SPDX-License-Identifier: BSD-2-Clause
-->

# renlace - rename files, replace strings within them

\[[Home][ringlet-home] | [GitLab][gitlab] | [crates.io][crates-io] | [ReadTheDocs][readthedocs]\]

## Overview

The `renlace` tool uses a directory tree as a template, then generates
a new tree with some files renamed and strings replaced within them as
specified.
Its main goal is to quickly set up a skeleton for a new programming
project according to a programming language-specific template.

The `renlace` tool uses [the Handlebars template language][handlebarsjs], so
any files with the `.hbs` extension found in the template tree will have
their contents processed as `Handlebars` templates.
The generated content will then be written to the respective file within
the new directory, but without the `.hbs` extension.
Thus, a `pyproject.toml.hbs` file containing placeholders for the project name
will result in a `pyproject.toml` file containing the specified project name.

## Contact

The `renlace` tool was written by [Peter Pentchev][roam].
It is developed in [a GitLab repository][gitlab]. This documentation is
hosted at [Ringlet][ringlet-home] with a copy at [ReadTheDocs][readthedocs].

[roam]: mailto:roam@ringlet.net "Peter Pentchev"
[gitlab]: https://gitlab.com/ppentchev/renlace "The renlace GitLab repository"
[crates-io]: https://crates.io/crates/renlace "The renlace crate on crates.io"
[readthedocs]: https://renlace.readthedocs.org/ "The renlace ReadTheDocs page"
[ringlet-home]: https://devel.ringlet.net/textproc/renlace/ "The Ringlet renlace homepage"
[handlebarsjs]: https://handlebarsjs.com/ "The Handlebars minimal template language"
