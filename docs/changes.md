<!--
SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
SPDX-License-Identifier: BSD-2-Clause
-->

# Changelog

All notable changes to the renlace project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Started

- First public release.

[Unreleased]: https://gitlab.com/ppentchev/renlace/-/compare/release%2F0.1.0...main
[0.1.0]: https://gitlab.com/ppentchev/renlace/-/tags/release%2F0.1.0
