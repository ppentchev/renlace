// SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
// SPDX-License-Identifier: BSD-2-Clause
//! Parse the `.renlace.toml` configuration file.

use std::collections::HashMap;
use std::fs;

use anyhow::{anyhow, Context as _};
use camino::Utf8Path;
use serde_derive::Deserialize;
use tracing::warn;

use crate::defs::Error;

/// Additional variables defined for this template.
#[derive(Debug, Default, Deserialize)]
pub struct ConfigTemplateVars {
    /// The names of any additional variables that may be specified.
    allowed: Vec<String>,

    /// The default values for some of these variables.
    #[serde(default)]
    defaults: HashMap<String, String>,
}

impl ConfigTemplateVars {
    /// The names of any additional variables that may be specified.
    #[inline]
    #[must_use]
    pub fn allowed(&self) -> &[String] {
        &self.allowed
    }

    /// The default values for some of these variables.
    #[inline]
    #[must_use]
    pub const fn defaults(&self) -> &HashMap<String, String> {
        &self.defaults
    }
}

/// Configuration for the `handlebars` engine.
#[derive(Debug, Deserialize)]
pub struct ConfigHandlebars {
    /// The filename extensions that will be processed by this engine.
    extensions: Vec<String>,
}

impl ConfigHandlebars {
    /// The filename extensions that will be processed by this engine.
    #[inline]
    #[must_use]
    pub fn extensions(&self) -> &[String] {
        &self.extensions
    }
}

/// The engine(s) that will be used for processing templates.
#[derive(Debug, Deserialize)]
pub struct ConfigEngine {
    /// Configuration for the `handlebars` engine.
    handlebars: Option<ConfigHandlebars>,
}

impl ConfigEngine {
    /// Configuration for the `handlebars` engine.
    #[inline]
    #[must_use]
    pub const fn handlebars(&self) -> &Option<ConfigHandlebars> {
        &self.handlebars
    }
}

/// Settings for this specific template.
#[derive(Debug, Deserialize)]
pub struct ConfigTemplate {
    /// The engine(s) that will be used for processing templates.
    engine: ConfigEngine,

    /// The string within the file and directory names to be replaced.
    pathname: String,

    /// Any additional variables that should maybe be defined.
    #[serde(default)]
    vars: ConfigTemplateVars,
}

impl ConfigTemplate {
    /// The engine(s) that will be used for processing templates.
    #[inline]
    #[must_use]
    pub const fn engine(&self) -> &ConfigEngine {
        &self.engine
    }

    /// The string within the file and directory names to be replaced.
    #[inline]
    #[must_use]
    pub fn pathname(&self) -> &str {
        &self.pathname
    }

    /// Any additional variables that should maybe be defined.
    #[inline]
    #[must_use]
    pub const fn vars(&self) -> &ConfigTemplateVars {
        &self.vars
    }
}

/// The template configuration settings for the `renlace` library.
#[derive(Debug, Deserialize)]
pub struct Config {
    /// Settings for this specific template.
    template: ConfigTemplate,
}

impl Config {
    /// Settings for this specific template.
    #[inline]
    #[must_use]
    pub const fn template(&self) -> &ConfigTemplate {
        &self.template
    }
}

/// The configuration file format version.
#[derive(Debug, Deserialize)]
struct ConfigFormatVersion {
    /// The major version number.
    major: u32,

    /// The minor version number.
    minor: u32,
}

/// The metadata for the configuration file format, mostly the version.
#[derive(Debug, Deserialize)]
struct ConfigFormat {
    /// The version of the format of the configuration file.
    version: ConfigFormatVersion,
}

/// The top-level configuration file format.
#[derive(Debug, Deserialize)]
struct ConfigTopV0 {
    /// The format metadata, mostly the version.
    format: ConfigFormat,

    /// The configuration for the `renplace` library itself.
    renlace: Config,
}

/// Parse the template configuration file.
///
/// # Errors
///
/// [`Error::FileRead`] if the configuration file could not be read.
/// [`Error::Format`] if the configuration file does not exist, is not a valid
/// TOML file, or does not have the expected structure.
#[inline]
#[tracing::instrument(skip_all)]
pub fn read_template_config<P: AsRef<Utf8Path>>(source_dir: &P) -> Result<Config, Error> {
    let config_path = source_dir.as_ref().join(".renlace.toml");
    let config_path_str = || config_path.to_string();
    let contents =
        fs::read_to_string(&config_path).map_err(|err| Error::FileRead(config_path_str(), err))?;

    let config_top = toml::from_str::<ConfigTopV0>(&contents)
        .context("Could not parse the configuration TOML file")
        .map_err(|err| Error::Format(config_path_str(), err))?;
    if config_top.format.version.major != 0 || config_top.format.version.minor == 0 {
        return Err(Error::Format(
            config_path_str(),
            anyhow!(
                "Unsupported config format version {major}.{minor}",
                major = config_top.format.version.major,
                minor = config_top.format.version.minor
            ),
        ));
    }
    if config_top.format.version.minor > 1 {
        warn!("The format version of the {config_path} file is higher than 0.1");
    }
    Ok(config_top.renlace)
}
