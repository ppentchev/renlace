// SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
// SPDX-License-Identifier: BSD-2-Clause
//! Examine the source template directory.

use std::collections::HashMap;
use std::fs;
use std::io::Write as _;
use std::process::{Command, Stdio};

use anyhow::{anyhow, Context as _};
use camino::{Utf8Path, Utf8PathBuf};
use handlebars::Handlebars;
use itertools::Itertools as _;
use tempfile::NamedTempFile;
use tracing::{debug, trace};

use crate::defs::{Config, Error};
use crate::parse;

/// The supported templating engines.
#[derive(Debug, Clone, Copy)]
#[non_exhaustive]
pub enum EngineType {
    /// No template substitution at all, copy the file as-is.
    FileCopy,

    /// The `handlebars` engine.
    Handlebars,
}

/// An input template: source directory, replacement settings.
#[derive(Debug)]
pub struct Template {
    /// The path to the template source directory.
    source_dir: Utf8PathBuf,

    /// The strings to replace within the project file and directory names.
    path_replace: HashMap<String, String>,

    /// The extension-to-engine mapping.
    engine_ext: HashMap<String, EngineType>,

    /// The variables to replace within the files.
    vars: HashMap<String, String>,
}

impl Template {
    /// The path to the template source directory.
    #[inline]
    #[must_use]
    pub fn source_dir(&self) -> &Utf8Path {
        &self.source_dir
    }

    /// The strings to replace within the project file and directory names.
    #[inline]
    #[must_use]
    pub const fn path_replace(&self) -> &HashMap<String, String> {
        &self.path_replace
    }

    /// The extension-to-engine mapping.
    #[inline]
    #[must_use]
    pub const fn engine_ext(&self) -> &HashMap<String, EngineType> {
        &self.engine_ext
    }

    /// The variables to replace within the files.
    #[inline]
    #[must_use]
    pub const fn vars(&self) -> &HashMap<String, String> {
        &self.vars
    }

    /// Create a new [`Template`] object from the configuration settings.
    ///
    /// # Errors
    ///
    /// [`Error::NotADirectory`] if the specified template path is not a directory.
    #[inline]
    pub fn new(cfg: &Config) -> Result<Self, Error> {
        let source_dir: Utf8PathBuf = cfg.template().into();
        if !source_dir.is_dir() {
            return Err(Error::NotADirectory(source_dir.to_string()));
        }

        // Read the .renlace.toml file for more info
        let tmpl_config = parse::read_template_config(&source_dir)?;

        let pathname = cfg.name().replace('-', "_");
        let path_replace = [(
            tmpl_config.template().pathname().to_owned(),
            pathname.clone(),
        )]
        .into();
        let vars = {
            let mut vars: HashMap<String, String> = [
                ("name".to_owned(), cfg.name().to_owned()),
                ("pathname".to_owned(), pathname),
            ]
            .into();

            if !cfg.vars().is_empty() {
                let mut undefined = cfg
                    .vars()
                    .keys()
                    .filter(|name| !tmpl_config.template().vars().allowed().contains(name))
                    .cloned()
                    .collect::<Vec<_>>();
                if !undefined.is_empty() {
                    undefined.sort_unstable();
                    return Err(Error::UnknownVars(undefined.join(", ")));
                }
            }
            vars.extend(
                cfg.vars()
                    .iter()
                    .map(|(name, value)| ((*name).clone(), (*value).clone())),
            );
            vars
        };

        let engine_ext = {
            let mut engine_ext = HashMap::new();

            if let Some(ref hbars) = *tmpl_config.template().engine().handlebars() {
                let hbar_ext = hbars
                    .extensions()
                    .iter()
                    .map(|ext| {
                        ext.strip_prefix('.').map_or_else(
                            || {
                                Err(Error::Format(
                                    source_dir.to_string(),
                                    anyhow!("The {ext:?} extension does not start with a dot"),
                                ))
                            },
                            |sext| Ok(((*sext).to_owned(), EngineType::Handlebars)),
                        )
                    })
                    .collect::<Result<Vec<_>, _>>()?;
                engine_ext.extend(hbar_ext);
            }

            engine_ext
        };

        Ok(Self {
            source_dir,
            path_replace,
            engine_ext,
            vars,
        })
    }

    /// Obtain the list of files to process relative to the template's source directory.
    ///
    /// # Errors
    ///
    /// [`Error::ListFiles`] on failure to list the files in the directory.
    /// [`Error::RunProgram`] if a spawned program, e.g. `git ls-files`, fails.
    #[inline]
    pub fn list_files(&self) -> Result<Vec<Utf8PathBuf>, Error> {
        // Unconditionally use `git ls-files`.
        // Maybe implement another method later.
        let output_lines = {
            let output = Command::new("git")
                .args(["ls-files", "."])
                .current_dir(&self.source_dir)
                .stdin(Stdio::null())
                .stdout(Stdio::piped())
                .stderr(Stdio::inherit())
                .output()
                .map_err(|err| Error::ListFiles(format!("{path}", path = self.source_dir), err))?;
            let progname = || format!("git ls-files {path}", path = self.source_dir);
            match output.status.code() {
                None => {
                    return Err(Error::Internal(anyhow!(
                        "Could not obtain an exit code for {prog}",
                        prog = progname()
                    )))
                }
                Some(0_i32) => (),
                Some(code) => return Err(Error::RunProgram(progname(), code)),
            };

            String::from_utf8(output.stdout)
                .with_context(|| {
                    format!(
                        "Could not parse the output of {prog} as valid UTF-8",
                        prog = progname()
                    )
                })
                .map_err(Error::Internal)?
        };
        output_lines
            .lines()
            .unique()
            .filter(|line| *line != ".renlace.toml")
            .map(|fpath| {
                let relpath = Utf8PathBuf::from(fpath);
                if relpath.is_relative() {
                    Ok(relpath)
                } else {
                    Err(Error::Internal(anyhow!(
                        "The {fpath} path returned by `git ls-files` was supposed to be relative"
                    )))
                }
            })
            .collect::<Result<_, _>>()
    }
}

/// Obtain a list of the template's files, replace their pathnames.
///
/// # Errors
///
/// Propagate errors from [`Template::list_files()`].
#[tracing::instrument(skip_all)]
fn scan_and_replace<PO: AsRef<Utf8Path>>(
    template: &Template,
    output_dir: &PO,
    path_replace: &HashMap<String, String>,
) -> Result<HashMap<Utf8PathBuf, EngineInput>, Error> {
    debug!(
        "About to scan {source_dir}",
        source_dir = template.source_dir()
    );
    template
        .list_files()?
        .into_iter()
        .map(|relpath| {
            trace!(name = "relpath", "{relpath:?}");
            let res_parts = relpath
                .iter()
                .map(|part| {
                    Ok(path_replace
                        .iter()
                        .fold(part.to_owned(), |acc, (key, value)| acc.replace(key, value)))
                })
                .collect::<Result<Vec<_>, _>>()?;
            let res_path = res_parts.into_iter().collect::<Utf8PathBuf>();

            if let Some(engine) = res_path
                .extension()
                .and_then(|ext| template.engine_ext().get(ext))
            {
                let stem_path = res_path.with_file_name(
                    res_path
                        .file_stem()
                        .with_context(|| {
                            format!("But we already extracted an extension from {res_path:?}")
                        })
                        .map_err(Error::Internal)?,
                );
                trace!(name = "stem_path", "{stem_path:?}");
                Ok((
                    output_dir.as_ref().join(&stem_path),
                    EngineInput::new(*engine, template.source_dir().join(&relpath)),
                ))
            } else {
                trace!(name = "res_path", "{res_path:?}");
                Ok((
                    output_dir.as_ref().join(&res_path),
                    EngineInput::new(EngineType::FileCopy, template.source_dir().join(&relpath)),
                ))
            }
        })
        .collect::<Result<_, _>>()
}

/// Replace placeholders in a file using the Handlebars templating system.
///
/// # Errors
///
/// [`Error::Handlebars`] on template processing errors.
fn replace_handlebars(
    hbars: &mut Handlebars<'_>,
    src_path: &str,
    contents: &str,
    vars: &HashMap<String, String>,
) -> Result<String, Error> {
    hbars
        .register_template_string("infile", contents)
        .context("Could not parse the Handlebars template")
        .map_err(|err| Error::Handlebars(src_path.to_owned(), err))?;
    hbars
        .render("infile", vars)
        .context("Could not render the Handlebars template using the provided variables")
        .map_err(|err| Error::Handlebars(src_path.to_owned(), err))
}

/// The source from which a single file will be generated.
#[derive(Debug)]
pub struct EngineInput {
    /// The engine that will generate the file.
    engine: EngineType,

    /// The path to the input file.
    source_path: Utf8PathBuf,
}

impl EngineInput {
    /// The engine that will generate the file.
    #[inline]
    #[must_use]
    pub const fn engine(&self) -> EngineType {
        self.engine
    }

    /// The path to the input file.
    #[inline]
    #[must_use]
    pub fn source_path(&self) -> &Utf8Path {
        &self.source_path
    }

    /// Create a new [`EngineInput`] object.
    #[inline]
    #[must_use]
    pub const fn new(engine: EngineType, source_path: Utf8PathBuf) -> Self {
        Self {
            engine,
            source_path,
        }
    }
}

/// The whole replacement project.
#[derive(Debug)]
pub struct Project {
    /// The template to generate the project from.
    template: Template,

    /// The directory to generate the project into.
    output_dir: Utf8PathBuf,

    /// The map of filenames to read and replace.
    files_map: HashMap<Utf8PathBuf, EngineInput>,
}

impl Project {
    /// The template to generate the project from.
    #[inline]
    #[must_use]
    pub const fn template(&self) -> &Template {
        &self.template
    }

    /// The directory to generate the project into.
    #[inline]
    #[must_use]
    pub fn output_dir(&self) -> &Utf8Path {
        &self.output_dir
    }

    /// The template to generate the project from.
    #[inline]
    #[must_use]
    pub const fn files_map(&self) -> &HashMap<impl AsRef<Utf8Path>, EngineInput> {
        &self.files_map
    }

    /// Create a new [`Project`] object.
    ///
    /// # Errors
    ///
    /// Propagate errors from [`Template::list_files()`].
    #[inline]
    pub fn new(cfg: &Config, template: Template) -> Result<Self, Error> {
        let output_dir = cfg.output_dir().into();
        let files_map = scan_and_replace(&template, &output_dir, template.path_replace())?;
        Ok(Self {
            template,
            output_dir,
            files_map,
        })
    }

    /// Create destination directories, copy files, replace their contents.
    ///
    /// # Errors
    ///
    /// [`Error::FileRead`] on filesystem errors.
    /// [`Error::Handlebars`] on template processing errors.
    #[inline]
    pub fn copy_and_replace(&self) -> Result<(), Error> {
        debug!(
            "Copying {count} files from {source_dir} to {output_dir}",
            count = self.files_map.len(),
            source_dir = self.template.source_dir(),
            output_dir = self.output_dir
        );

        let mut hbars = {
            let mut hbars = Handlebars::new();
            hbars.set_strict_mode(true);
            hbars
        };

        for (dst_path, engine_input) in self.files_map.iter().sorted_unstable_by_key(|item| item.0)
        {
            let src_path = engine_input.source_path();
            trace!(name = "src_path", "{src_path:?}");
            trace!(name = "dst_path", "{dst_path:?}");
            let src_path_str = || src_path.to_string();
            let dst_path_str = || dst_path.to_string();

            let contents =
                fs::read_to_string(src_path).map_err(|err| Error::FileRead(src_path_str(), err))?;
            let replaced = match engine_input.engine() {
                EngineType::Handlebars => replace_handlebars(
                    &mut hbars,
                    src_path.as_ref(),
                    &contents,
                    self.template.vars(),
                )?,
                EngineType::FileCopy => contents,
            };

            let dst_dir = dst_path
                .parent()
                .ok_or_else(|| Error::Internal(anyhow!("Is {dst_path} an empty path?")))?;
            let dst_dir_str = || dst_dir.to_string();
            fs::create_dir_all(dst_dir).map_err(|err| Error::DirCreate(dst_dir_str(), err))?;
            let mut tempf = NamedTempFile::new_in(dst_dir)
                .map_err(|err| Error::FileTempCreate(dst_dir_str(), err))?;
            tempf
                .write_all(replaced.as_bytes())
                .map_err(|err| Error::FileTempWrite(dst_dir_str(), err))?;
            fs::set_permissions(
                &tempf,
                fs::metadata(src_path)
                    .map_err(|err| Error::FileRead(src_path_str(), err))?
                    .permissions(),
            )
            .map_err(|err| Error::FileTempWrite(dst_dir_str(), err))?;
            tempf
                .persist(dst_path)
                .map_err(|err| Error::FileTempRename(dst_path_str(), err.error))?;
        }
        Ok(())
    }
}
