// SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
// SPDX-License-Identifier: BSD-2-Clause
//! Common definitions for the renlace library.

use std::collections::HashMap;
use std::io::Error as IoError;

use anyhow::Error as AnyError;
use camino::{Utf8Path, Utf8PathBuf};
use thiserror::Error;

/// Errors that may occur during the file and directory processing.
#[derive(Debug, Error)]
#[non_exhaustive]
#[allow(clippy::error_impl_error)]
pub enum Error {
    /// A directory could not be created.
    #[error("Could not create the {0} directory")]
    DirCreate(String, #[source] IoError),

    /// A file could not be read.
    #[error("Could not read the {0} file")]
    FileRead(String, #[source] IoError),

    /// A temporary file could not be created.
    #[error("Could not create a temporary file in the {0} directory")]
    FileTempCreate(String, #[source] IoError),

    /// A temporary file could not be renamed.
    #[error("Could not rename a temporary file to {0}")]
    FileTempRename(String, #[source] IoError),

    /// Could not write to the temporary file.
    #[error("Could not write to a temporary file in the {0} directory")]
    FileTempWrite(String, #[source] IoError),

    /// A file did not have the expected format.
    #[error("Unexpected format for the {0} file")]
    Format(String, #[source] AnyError),

    /// The Handlebars template system returned an error.
    #[error("Handlebars template error processing the {0} file")]
    Handlebars(String, #[source] AnyError),

    /// Something went really, really wrong.
    #[error("renlace internal error")]
    Internal(#[source] AnyError),

    /// Could not list the files within a directory.
    #[error("Could not list the files within the {0} directory")]
    ListFiles(String, #[source] IoError),

    /// We expected something to be a directory...
    #[error("Not a directory: {0}")]
    NotADirectory(String),

    /// A program indicated a failure.
    #[error("The `{0}` program exited with code {1}")]
    RunProgram(String, i32),

    /// Unknown variable names specified in the configuration.
    #[error("Unknown variable names: {0}")]
    UnknownVars(String),
}

/// Runtime configuration for the renlace library functions.
#[derive(Debug)]
pub struct Config {
    /// The template to use to generate the files.
    template: Utf8PathBuf,

    /// The output directory to put the files in.
    output_dir: Utf8PathBuf,

    /// The name of the project to generate.
    name: String,

    /// Additional template-specific variables.
    vars: HashMap<String, String>,

    /// Verbose operation; display diagnostic output.
    verbose: bool,
}

impl Config {
    /// The template to use to generate the files.
    #[inline]
    #[must_use]
    pub fn template(&self) -> &Utf8Path {
        &self.template
    }

    /// The output directory to put the files in.
    #[inline]
    #[must_use]
    pub fn output_dir(&self) -> &Utf8Path {
        &self.output_dir
    }

    /// The name of the project to generate.
    #[inline]
    #[must_use]
    pub fn name(&self) -> &str {
        &self.name
    }

    /// Additional template-specific variables.
    #[inline]
    #[must_use]
    pub const fn vars(&self) -> &HashMap<String, String> {
        &self.vars
    }

    /// Verbose operation; display diagnostic output.
    #[inline]
    #[must_use]
    pub const fn verbose(&self) -> bool {
        self.verbose
    }

    /// Create a new [`Config`] object with the specified template, directory, and name.
    #[inline]
    #[must_use]
    pub fn new(template: Utf8PathBuf, output_dir: Utf8PathBuf, name: String) -> Self {
        Self {
            template,
            output_dir,
            name,
            verbose: false,
            vars: HashMap::new(),
        }
    }

    /// Consume the object, return a new one with the specified verbosity setting.
    #[inline]
    #[must_use]
    pub fn with_verbose(self, verbose: bool) -> Self {
        Self { verbose, ..self }
    }

    /// Consume the object, return a new one with the specified additional variables.
    #[inline]
    #[must_use]
    pub fn with_vars(self, vars: HashMap<String, String>) -> Self {
        Self { vars, ..self }
    }
}
