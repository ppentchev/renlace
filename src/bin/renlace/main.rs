#![deny(missing_docs)]
#![deny(clippy::missing_docs_in_private_items)]
// SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
// SPDX-License-Identifier: BSD-2-Clause
//! Copy a directory tree, rename files, replace strings within them.

use std::process::{ExitCode, Termination};

use anyhow::{Context as _, Result};

use renlace::{Config, Project, Template};

mod cli;

use cli::Mode;

/// The result of the main program or an invoked subcommand.
enum MainResult {
    /// Everything seems fine.
    Ok,
}

impl Termination for MainResult {
    fn report(self) -> ExitCode {
        match self {
            Self::Ok => ExitCode::SUCCESS,
        }
    }
}

/// Do the actual copying of files, etc.
fn run(cfg: &Config) -> Result<MainResult> {
    let project = {
        let template = Template::new(cfg).context("Could not prepare the source template")?;
        Project::new(cfg, template).context("Could not prepare the replace project")?
    };
    project
        .copy_and_replace()
        .context("Could not copy files and/or replace their contents")?;
    Ok(MainResult::Ok)
}

/// The main program - parse command-line options, do something about it.
fn main() -> Result<MainResult> {
    match cli::try_parse()? {
        Mode::Handled => Ok(MainResult::Ok),
        Mode::Run(cfg) => run(&cfg),
    }
}
