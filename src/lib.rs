#![deny(missing_docs)]
#![deny(clippy::missing_docs_in_private_items)]
// SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
// SPDX-License-Identifier: BSD-2-Clause
//! Replace strings in files, rename the files.

#![allow(clippy::pub_use)]

pub mod defs;
pub mod parse;
pub mod template;

pub use defs::Config;
pub use template::{Project, Template};
