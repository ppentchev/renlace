# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause

{ pkgs ? import <nixpkgs> { } }:
pkgs.mkShell {
    buildInputs = [
        pkgs.cargo
        pkgs.clippy
        pkgs.rustfmt
    ];
    shellHook = ''
        set -e
        cargo fmt --check
        env NIX_ENFORCE_PURITY=0 cargo build
        env NIX_ENFORCE_PURITY=0 cargo doc --no-deps
        cargo clippy
        env NIX_ENFORCE_PURITY=0 cargo test -- --nocapture
        exit
    '';
}
