# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause

{ pkgs ? import <nixpkgs> { } }:
pkgs.mkShell {
    buildInputs = [
        pkgs.cargo
        pkgs.clippy
    ];
    shellHook = ''
        if ! ./run-clippy.sh -n; then
            echo 'clippy failed' 1>&2
        fi
        true
        exit
    '';
}
